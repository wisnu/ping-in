<?php

/*
Plugin Name: Ping-in
Description: This plugin don't do anything...
Plugin URI: http://wis.nu
Author: Wisnu
Author URI: http://wis.nu
Version: 0.0.1

*/
      if( in_array(date('i'), [ 0, 10, 20, 30, 40, 50, 59 ] ) )
      {
        $sitemap = home_url('sitemap.xml');
        $url = "http://www.google.com/webmasters/sitemaps/ping?sitemap=".$sitemap;
        pinger($url);
        $url = "http://www.bing.com/webmaster/ping.aspx?siteMap=".$sitemap;
        pinger($url);
      }


function pinger( $url = '' ){
  if( empty($url) ) {
    $url = home_url( $url );
  }
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_exec($ch);
  curl_close($ch);
  return true;
}
